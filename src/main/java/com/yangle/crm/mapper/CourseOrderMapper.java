package com.yangle.crm.mapper;

import com.yangle.crm.bean.CourseOrder;

import java.util.List;
import java.util.Map;

/**
 * company: www.abc.com
 * Author: 10635
 * Create Data: 2020/11/23
 */
public interface CourseOrderMapper {
    public List<CourseOrder> findListOrder(Map<String,Object> map);
    public int getAllOrders();
    public int saveOrder(CourseOrder courseOrder);
    public CourseOrder getCourseOderById(String order_id);
}
