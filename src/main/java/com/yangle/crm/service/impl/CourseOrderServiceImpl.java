package com.yangle.crm.service.impl;

import com.yangle.crm.bean.CourseOrder;
import com.yangle.crm.dto.PageResult;
import com.yangle.crm.mapper.CourseOrderMapper;
import com.yangle.crm.service.CourseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * company: www.abc.com
 * Author: 10635
 * Create Data: 2020/11/23
 */
@Service
public class CourseOrderServiceImpl implements CourseOrderService {

    @Autowired
    private CourseOrderMapper courseOrderMapper;

    @Override
    public PageResult<CourseOrder> getOrderList(int page,int pageSize) {
        Map<String,Object> map = new HashMap<>();
        map.put("startSize", (page-1)*pageSize);
        map.put("endSize", pageSize);
        PageResult<CourseOrder> pageResult = new PageResult<>();
        pageResult.setCode(0);
        List<CourseOrder> list = courseOrderMapper.findListOrder(map);
        int count = courseOrderMapper.getAllOrders();
        pageResult.setCount(count);
        pageResult.setMsg("success");
        pageResult.setData(list);
        return pageResult;
    }

    @Override
    public int getAllOrders() {
        return courseOrderMapper.getAllOrders();
    }

    @Override
    public int addCourseOrder(CourseOrder courseOrder) {
        String uid = UUID.randomUUID().toString();
        courseOrder.setUuid(uid);
        return courseOrderMapper.saveOrder(courseOrder);
    }

    @Override
    public CourseOrder getOrderById(String order_id) {
        CourseOrder courseOrder = courseOrderMapper.getCourseOderById(order_id);

        return courseOrder;
    }
}
