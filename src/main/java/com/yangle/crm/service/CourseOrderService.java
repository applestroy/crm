package com.yangle.crm.service;

import com.yangle.crm.bean.CourseOrder;
import com.yangle.crm.dto.PageResult;

import java.util.Map;

/**
 * company: www.abc.com
 * Author: 10635
 * Create Data: 2020/11/23
 */
public interface CourseOrderService {

    public PageResult<CourseOrder> getOrderList(int page,int pageSize);
    public int getAllOrders();
    public int addCourseOrder(CourseOrder courseOrder);
    public CourseOrder getOrderById(String order_id);
}
