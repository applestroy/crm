package com.yangle.crm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * company: www.abc.com
 * Author: 10635
 * Create Data: 2020/11/19
 */
@Controller
public class IndexController {

    @RequestMapping("/index")
    public String welcome(){
        return "index";
    }


}
