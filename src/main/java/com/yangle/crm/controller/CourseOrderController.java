package com.yangle.crm.controller;

import com.yangle.crm.bean.CRUDReslut;
import com.yangle.crm.bean.CourseOrder;
import com.yangle.crm.dto.PageResult;
import com.yangle.crm.service.CourseOrderService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.util.StringUtils;

/**
 * company: www.abc.com
 * Author: 10635
 * Create Data: 2020/11/23
 */
@Controller
@RequestMapping("/courseOrder")
public class CourseOrderController {

    @Autowired
    private CourseOrderService courseOrderService;
    @RequestMapping("/list")
    public String listCourse(){
        System.out.println("listOrder");
        return "courseOrder/listOrder";
    }
    @RequestMapping("/add")
    public String addCourse(){
        System.out.println("addOrder");
        return "courseOrder/addOrder";
    }
    @RequestMapping("/getOrderList")
    @ResponseBody
    public Object getOrderList(int page,int limit){
        System.out.println("page:"+page);
        System.out.println("page:"+limit);
        PageResult<CourseOrder> result =  courseOrderService.getOrderList(page,limit);
        return result;
    }

    @RequestMapping("/addOrder")
    @ResponseBody
    public Object addCourseOder(CourseOrder courseOrder){
        CRUDReslut reslut = new CRUDReslut();
        int i = courseOrderService.addCourseOrder(courseOrder);
        reslut.setSuccess(i);
        if(i==0){
            reslut.setMsg("生成对象失败，请联系管理员");
        }
        System.out.println(i);
        return reslut;
    }

    @RequestMapping("/detail")
    public String courseDetail(Model model, @RequestParam("order_id") String course_id){
        System.out.println("course:"+course_id);
        CourseOrder courseOrder  = new CourseOrder();
        if (StringUtils.isEmpty(course_id)){
            System.out.println("enter empty");
            model.addAttribute("order", courseOrder);
        }else{

            courseOrder = courseOrderService.getOrderById(course_id);
            model.addAttribute("order", courseOrder);
        }


        return "courseOrder/orderDetail";
    }
}
