package com.yangle.crm.bean;

/**
 * company: www.abc.com
 * Author: 10635
 * Create Data: 2020/11/26
 */
public class CRUDReslut {
    private int success =1;
    private String msg;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public CRUDReslut(int success, String msg) {
        this.success = success;
        this.msg = msg;
    }

    public CRUDReslut() {
    }

    @Override
    public String toString() {
        return "CRUDReslut{" +
                "success=" + success +
                ", msg='" + msg + '\'' +
                '}';
    }
}
